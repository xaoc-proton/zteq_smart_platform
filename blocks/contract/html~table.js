
exports.readme = {
	title: 'Список договоров',
	img: 'contract_list.png',
}

exports.tpl = (_, d, c = {})=>{

	if(c.hasParent === undefined) c.hasParent = true;

return [

	_.html('__tpl~table', _, d, {
		hasParent: c.hasParent,
		col: 'contract', 
		label: 'Договор c '+d.name,
		filter: c.filter != undefined ? c.filter : {
			items: [
				{class: "col-xs-3", f: {name: 'find_text', value: '', label: 'Поиск по контрагентам'}},
				{class: "col-xs-2", f: {name: 'find_from', type: 'date+', label: 'Заключенного с'}},
				{class: "col-xs-2", f: {name: 'find_to', type: 'date+', label: 'по'}},
				{class: "col-xs-2", f: {name: 'find_status', type: 'select', lst: 'contract~status', label: 'Статус'}},
				//{class: "col-xs-2", f: {name: 'is_delete', type: 'check_btn+', label: 'Показывать\x0aудаленные'}},
			],
		},
		add: {
			modal: true,
			btn: {
				label: 'Новый Договор'
			},
			items: [
				{f: {name: 'num', label: "Номер договора"}},
				c.hasParent ? {}
					: {f: {name: 'clientLink', label: "Заказчик", type: 'select2', lst: 'ce', ajax: true}},
				{f: {name: 'executorLink', label: "Исполнитель", type: 'select2', lst: 'ce', ajax: true}},
				{f: {name: 'type', label: "Тип", type: 'select', lst: 'contract~type'}},
				{f: {name: 'start_date', label: "Дата заключения", type: 'date+', value: ''}},
				{f: {name: 'comm', label: "Комментарий", type: 'textarea', value: ''}},
				{f: {name: 'scan', label: "Скан договора", type: 'file', value: ''}},
			],
			afterValid: async (conn, data, parents, callback)=>{

				if(!data.tmp_obj.executorLink?.v)
					return callback({status: 'err', err: 'Исполнитель должен быть указан'});

				if(data.field.parent.config.hasParent){
					const parent = await conn.db.collection('ce').findOne({ _id: ObjectId(parents.find(item => item.col === 'ce')?._id) }, {name: 1});
					data.tmp_obj.clientLink = {v: parent?._id?.toString(), l: parent?.name};
				}

				if(!data.tmp_obj.clientLink?.v)
					return callback({status: 'err', err: 'Заказчик должен быть указан'});

				if(data.tmp_obj.executorLink.v === data.tmp_obj.clientLink.v){
					return callback({status: 'err', err: 'Заказчик и исполнитель должны быть разными юр.лицами'});
				}else{
					return callback();
				}
			},
			beforeAdd: (conn, data, parents, callback)=>{
				data.tmp_obj.status = LST['contract~status'].defval;
				parents.push({col: 'ce', _id: data.tmp_obj.clientLink.v});
				parents.push({col: 'ce', _id: data.tmp_obj.executorLink.v});
				callback();
			},
		},
		table: {
			itemLink: {
				type: 'col',
			},
			// prepareItem: (_, d, item)=>{ 
			// 	if(d.delete_time){
			// 		item[1].class = (item[1].class||'')+' deleted';
			// 		item[1].help = 'Уволен '+moment(d.delete_time).format('LL');
			// 	}
			// 	return item;
			// },
			checkItems: true,
			items: [
				{class: 'hidden', f: {name: 'delete_time', type: 'datetime--', value: ''}},
				{label: 'Добавлен', f: {name: 'add_time', type: 'datetime--', value: ''}},
				{label: 'Номер', f: {name: 'num', type: 'label'}},
				//{label: 'Статус', f: {name: 'status', type: 'select--', lst: 'status~type'}},
				{label: 'Статус', html: (_, d)=>[
					['span', {text: LST['contract~type']?.list.obj[d.status]?.l}],
					_.f({name: 'status', type: 'select--', lst: 'contract~status'}),
				]},
				{label: 'Дата заключения', f: {name: 'start_date', type: 'date--', value: ''}},
				{label: 'Заказчик', f: {name: 'clientLink', type: 'select2--', lst: 'ce'}},
				{label: 'Исполнитель', f: {name: 'executorLink', type: 'select2--', lst: 'ce'}},
			],
			id: {
				baseField: 'add_time',
				prepare: (select)=>{
					// select.join.push("LEFT JOIN worker__delete_time ON worker__delete_time.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
					// if(!select.filter.is_delete) select.where.push("worker__delete_time.f = 0");
					
					if(select.filter.find_from){
						select.where.push(select.config.col+"__"+select.config.table.id.baseField+".f >= ?");
						select.escape.push(moment(select.filter.find_from).unix()*1000);
					}
					if(select.filter.find_to){
						select.where.push(select.config.col+"__"+select.config.table.id.baseField+".f < ?");
						select.escape.push(moment(select.filter.find_to).add(1,'day').unix()*1000);
					}

					if(select.filter.find_text){
						select.join.push("LEFT JOIN contract_links__ce ON contract_links__ce.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
						select.join.push("LEFT JOIN ce__name ON ce__name.id = contract_links__ce.p");
						select.where.push("ce__name.f LIKE ?");
						select.escape.push('%'+select.filter.find_text.trim()+'%');
					}
					if(select.filter.find_status){
						select.join.push("LEFT JOIN contract__status ON contract__status.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
						select.where.push("contract__status.f = ?");
						select.escape.push(select.filter.find_status);
					}

				},
			},
		}
	}),
]} 