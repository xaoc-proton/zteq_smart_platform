
exports.config = {
	menu: {label: 'Контракты', icon: 'fas fa-address-book'},
}

exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	allow: ['contract']
}, callback);

exports.id = async (__, code, callback)=>{

	if(__.user.workerCompany){
		__.fields[code].col = 'ce';
		__.fields[code].link = ['__ce'];
		__.queryIds[code] = [__.user.workerCompany];
	}else{
		__.fields[code].col = 'user';
		__.fields[code].link = ['__'+__.fields[code].col];
		__.queryIds[code] = [__.user.key];
	}
	
	callback();
}
exports.tpl = (_, d)=>{ return [
	
	_.html('__tpl~list', _, d, {
		content: (_, d)=>[
			_.html('contract~table', _, d, {
				filter: _.__.user.role === 'main' ? {} : undefined,
				hasParent: _.__.user.role === 'main' ? true : false
			}),
		]
	}),
]}

exports.script = ()=>{

}

exports.style = ()=>{/*

*/}