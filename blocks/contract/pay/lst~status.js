
exports.config = {
	multi: true,
}

exports.lst = ()=>{
	var lst = [
		{v: '', l: 'Все статусы платежа'},
		{v: 'draft', l: 'Потенциальный'},
		{v: 'wait', l: 'Ожидаемый'},
		{v: 'ready', l: 'Полученный'},
		{v: 'cancel', l: 'Отмененный'},
		{v: 'null', l: 'Потерянный'},
	];
}