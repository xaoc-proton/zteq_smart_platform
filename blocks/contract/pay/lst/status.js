
	var lst = [
		{v: '', l: 'Все статусы платежа'},
		{v: 'draft', l: 'Потенциальный'},
		{v: 'wait', l: 'Ожидаемый'},
		{v: 'ready', l: 'Полученный'},
		{v: 'cancel', l: 'Отмененный'},
		{v: 'null', l: 'Потерянный'},
	];
if(typeof exports != 'undefined') exports.lst = lst;
if(typeof window != 'undefined') window.LST['contract/pay~status'] = lst;