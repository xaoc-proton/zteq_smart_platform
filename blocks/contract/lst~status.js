
exports.config = {
	multi: true,
}

exports.lst = ()=>{
	var lst = [
		{v: '', l: 'Статус не выбран'},
		{v: 'send', l: 'Заказчик направил заказ', owner: {role: 'main', company: 'executor'}, defval: true},
		{v: 'get', l: 'Поставщик принял заказ', owner: {role: 'head'}},
		{v: 'accept_order', l: 'Банк акцептовал заказ', owner: {role: 'main', company: 'executor'}},
		{v: 'ready', l: 'Поставщик выполнил заказ', owner: {role: 'main', company: 'client'}},
		{v: 'confirm', l: 'Заказчик принял заказ', owner: {role: 'head'}},
		{v: 'accept_work', l: 'Банк акцептовал выполненные работы', owner: {role: 'head'}},
		{v: 'docs', l: 'Сформировались документы на проведения платежа'},
	];
}