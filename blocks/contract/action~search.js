
exports.api = {
	alias: ['search_contract'],
}

exports.f = function(conn, data, msg, callback){try{
	
	if(msg.q){
		
		var res = {};
		
		async.waterfall([/*(cb)=>{
			DB.session.getParent(conn, data.parent.parent, res, cb);
		}*/], (err)=>{try{
			
			if(err){
				callback(err);
			}else{

				var sql = `(
					SELECT 
						contract_links__client.id _id, contract_links__client.p cid 
					FROM 
						client__name, contract_links__client 
					WHERE 
						client__name.id = contract_links__client.p AND client__name.f LIKE ?
				) UNION DISTINCT (
					SELECT 
						contract__num.id _id, '' cid 
					FROM 
						contract__num 
					WHERE 
						contract__num.f LIKE ?
				)`, escape = ["%"+msg.q+"%", "%"+msg.q+"%"];
				
				if(data.parent.col == 'point'){
					sql = `
						SELECT 
							contract_links__client.id _id, contract_links__client.p cid 
						FROM 
							point_links__client, contract_links__client 
						WHERE 
							point_links__client.p = contract_links__client.p AND point_links__client.id = ?
					`;
					escape = [data.parent._id];
				}

				DB.select(conn, {sql: sql, escape: escape}, (data)=>{

					if(!data.length){
						callback({results: []});
					}else{
						
						var results = [], w = [], text = {}, context = {};
						
						var contractIds = data.filter(d=>d._id&&d._id!==true).map(d=>ObjectId(d._id));
						var clientIds = data.filter(d=>d.cid&&d.cid!==true).map(d=>ObjectId(d.cid));
						
						w.push((cb)=>{
							conn.db.collection('contract')
							.find({_id: {$in: contractIds}}, {num: 1, __client: 1, __point: 1})
							.toArray((err, contract)=>{
								(contract||[]).forEach(_=>{
									context[_._id] = {t: _.num || ''};
									if(!_.cid){
										clientIds = clientIds.concat(SYS.get(_, '__client.l')||[]);
										context[_._id].cid = SYS.get(_, '__client.l[0]')+'';
									}
									context[_._id].points = SYS.get(_, '__point.l.length');
								});
								cb();
							});
						});
						
						w.push((cb)=>{
							conn.db.collection('client')
							.find({_id: {$in: clientIds}}, {name: 1})
							.toArray((err, client)=>{
								if(client && client.length) client.forEach(_=>{ text[_._id] = {t: _.name || ''}});
								cb();
							});
						});
						
						async.waterfall(w, (err, res)=>{
							
							data.forEach((d)=>{
								if((d._id && d._id !== true) || msg.addEmpty){
									results.push({
										id: d._id && d._id !== true ? d._id : ('process_'+JSON.stringify({cid: d.cid})),
										text: 'Договор № '+((context[d._id]||{}).t||'б/н')+' c '+((text[d.cid]||{}).t||(text[(context[d._id]||{}).cid]||{}).t||'')+' ('+((context[d._id]||{}).points||'0')+' шт.)'
									});
								}
							})
							
							callback({results: results});
						});
					}
				});			
			}
		}catch(e){ console.log(e); callback({status: 'err', err: 'Ошибка поиска(2)'}) }});
	}else{
		callback({status: 'err', err: 'Строка поиска не может быть пустой.'});
	}
}catch(e){ console.log(e); callback({status: 'err', err: 'Ошибка поиска(1)'}) }}