exports.config = {
	multi: true,
}

exports.html = {
	tpl: (_, d)=>{

		var linkLabel = 'Наименование', linkLst = '';
		
		switch((_.config||{}).form){
			case 'client':
				linkLabel += ' подразделения';
				linkLst = 'partner';
				break;
			case 'partner':
				linkLabel += ' клиента';
				linkLst = 'client';
				break;
		}

	return [

		["div",{"class": "modal-dialog","role": "document"},[
			
			["div",{"class": "modal-content"},[
				
				["div",{"class": "modal-header"},[
					["button",{"type": "button","class": "close","data-dismiss": "modal","aria-label": "Close"},[
						["span",{"aria-hidden": "true"},[
							["i",{"class": "fas fa-times"}]
						]]
					]],
					["h4",{"class": "modal-title"},[
						["span", {"text": "Новый договор"}],
					]]
				]],
				
				["div",{"class": "modal-body"},[
					["div",{"role": "form", class: 'm-t *css*', style:()=>{/*css
						.*css* .el > .select2 {
							min-width: 100%;
						}
					css*/}},[
						
						["div",{"class": "row"},[
							["div",{"class": "col-xs-12"},[
								
								["div",{class: 'form-group'},[
									["label",{},[
										["span",{"text": 'Номер договора'}],
									]],
									_.f({name: 'num'}),
								]],

								["div",{class: 'form-group'},[
									["label",{class:''+`css
										.*css* {
											display: flex;
										}
										.*css* > .el > input {
											display: none;
										}
										.*css* > .el {
											text-align: right;
										}
										.*css* > .el > input + label {
											color: #888;
											opacity: 0.5;
										}
										.*css* > .el > input:checked + label {
											opacity: 1;
										}
									`},[
										["span",{"text": "Связанный\xa0договор"}],
										_.if(linkLst == 'partner', ()=>[
											_.f({name: 'link_only', type: 'check', label: 'Без создания договора'}),
										]),
									]],
									_.f({name: 'contractLink', type: 'select2', lst: 'contract', ajax: true}),
								]],
								
								_.if(linkLst, ()=>[
									["div",{class: 'form-group'},[
										["label",{},[
											["span",{"text": linkLabel}]
										]],
										_.f({name: 'link', type: 'select2', lst: linkLst, ajax: true}),
									]],
								]),
								_.if(!linkLst, ()=>[
									["div",{class: 'form-group'},[
										["label",{},[
											["span",{"text": "Наименование подразделения"}]
										]],
										_.f({name: 'partnerLink', type: 'select2', lst: 'partner', ajax: true, value: SYS.get(_.__, 'user.role_link')}),
									]],
									["div",{class: 'form-group'},[
										["label",{},[
											["span",{"text": "Наименование клиента"}]
										]],
										_.f({name: 'clientLink', type: 'select2', lst: 'client', ajax: true}),
									]],
								]),
								["div",{class: 'form-group'},[
									["label",{},[
										["span",{"text": "Тип"}]
									]],
									_.f({name: 'type', type: 'select', lst: 'contract~type'}),
								]],
								["div",{class: 'form-group'},[
									["label",{},[
										["span",{"text": "Дата заключения"}]
									]],
									_.f({name: 'start_date', type: 'date+', value: ''}),
								]],
								_.if(d.edit, ()=>[
									["div",{class: 'form-group'},[
										["label",{},[
											["span",{"text": "Дата расторжения"}]
										]],
										_.f({name: 'end_date', type: 'date+', value: ''}),
									]],	
								]),
							]],
							["div",{"class": "col-xs-6 hidden"},[
								
								["div",{class: 'form-group'},[
									["label",{},[
										["span",{"text": 'Скан договора'}],
										["div",{class: (d.scan?'':'hidden')+' h *css*', style:()=>{/*css
												position: absolute;
												height: 50px;
												top: 0px;
												right: 20px;
												color: #888;
										css*/}},[							
											//_.f({name: 'scan', type: 'imgbtn-', label: 'Скачать'}),
										]],
									]],
									["div",{class: '*css*', style:()=>{/*css
										position: relative;
										height: 300px;
										margin-bottom: 40px;
									css*/}},[
										_.f({name: 'scan', type: 'img+'}),
									]],
								]],
							]],
						]],
						["div",{class: 'form-group'},[
							["label",{},[
								["span",{"text": "Комментарий"}]
							]],
							_.f({name: 'comm', type: 'textarea', value: ''}),
						]],
						["div",{class: 'form-group *css*', style:()=>{/*css
							.*css* > .el {
								display: flex;
							}
							.*css* > .el > input {
								width: 30px;
							}
						css*/}},[
							["label",{},[
								["span",{"text": "Услуги"}]
							]],
							LST['contract~service'].list.lst.map(l=>[
								l.v ? _.f({name: 'service.'+l.v, type: 'check', label: l.l}) : '',
							]),
						]],
					]]
				]],
				["div",{"class": "modal-footer"},[
					["button",{"type": "button","class": "btn btn-default","data-dismiss": "modal"},[
						["span",{"text": "Отменить"}]
					]],
					["button",{"type": "button","class": "btn btn-primary"},[
						["span",{"text": "Создать договор"}],
						_.f({name: 'contract~add', type: 'action', front: {
							onAction: ()=>location.reload(),
						}}),
					]]
				]]
			]]
		]]
	]},
}

exports.action = {
	f: function(conn, data, msg, callback){
		
		var res = {};
		
		async.waterfall([cb=>{
			DB.session.getParent(conn, data.parent.parent, res, cb);
		}, cb=>{
			conn.db.collection('tmp_obj').findOne(ObjectId(data.parent._id), {}, (err, obj)=>{
				
				if(!obj.partnerLink) obj.partnerLink = SYS.get(conn.user, 'role_link');
				
				if(obj.link || (res.parent.col == 'user' && obj.partnerLink && obj.clientLink)){
					res.obj = obj;
					cb();
				}else{
					cb({status: 'err', err: 'Субъект договора должен быть выбран'});
				}
			});
		}, cb=>{
			conn.db.collection('user').findOne(ObjectId(conn.user.key), {login: 1, __pp: 1}, (err, user)=>{
				conn.db.collection('pp').findOne(SYS.get(user, '__pp.l[0]'), {second_name: 1, first_name: 1}, (err, pp)=>{
					res.user = pp || {};
					res.obj.add_user = ((pp.second_name||'')+' '+(pp.first_name||'')).trim() || '';
					cb();
				});
			});
		}, cb=>{try{
			conn.db.collection('client').findOne(ObjectId(res.parent.col == 'user' ? res.obj.clientLink.v : res.parent.col == 'client' ? res.parent._id : res.obj.link.v), {}, (err, client)=>{
				res.client = client;
				cb();
			});
		}catch(e){ cb({status: 'err', err: 'Ошибка поиска субъекта договора'}) }}, cb=>{
			
			data.parent.deleteLinks = true;
			
			if(res.parent.col == 'user'){
				data.parent.links = {
					tmp_obj: {user: '__user'},
					user: '__tmp_obj_contract',
				}
			}else{			
				data.parent.links = {tmp_obj: {}}
				data.parent.links.tmp_obj[res.parent.col] = '__'+res.parent.col;
				data.parent.links[res.parent.col] = '__'+res.field.name;
			}

			DB.deleteComplex(conn, data.parent, [{col: res.parent.col, _id: res.parent._id}], ()=>{ cb() });
			
		}, cb=>{
			
			conn.db.collection('client').findAndModify({ _id: res.client._id }, [], {$inc: {'counters.contract': 1}}, {fields: {counters: 1}}, (err, findData)=>{
			
				var parents = [];
				
				if(res.parent.col == 'user'){
					parents.push({_id: res.obj.partnerLink.v, col: 'partner'});
					parents.push({_id: res.obj.clientLink.v, col: 'client'});
				}else{
					parents.push({_id: res.parent._id, col: res.parent.col});
					parents.push({_id: res.obj.link.v, col: res.parent.col == 'client' ? 'partner' : 'client'});
				}
				
				if(res.obj.contractLink) parents.push({_id: res.obj.contractLink.v, col: 'contract', childLink: false});
			
				delete res.obj.link;
				delete res.obj.partnerLink;
				delete res.obj.clientLink;
				delete res.obj.add_time;
				delete res.obj._id;
				
				res.obj.code = res.client.code+'-'+((SYS.get(findData, 'value.counters.contract')||0) + 1);
				
				if(res.obj.link_only && res.obj.contractLink){
					parents = [{_id: res.parent._id, col: res.parent.col}];
					DB.addComplex(conn, {col: 'contract', _id: res.obj.contractLink.v}, parents, ()=>{ cb() });
				}else{
					DB.addComplex(conn, {col: 'contract'}, parents, res.obj, ()=>{ cb() });
				}
			});
		}], (err)=>{ callback(err || {status: 'ok'}) });
	}
}