
exports.access = (__, data, callback) => SYS.requireFile('func', 'user~access').f(__, {
	allow: ['contract']
}, callback);

exports.readme = {
	title: 'Форма работы с договорами',
	img: 'contract_form.png',
}

exports.id = (__, code, callback) => {
	__.fields[code].col = 'contract';
	__.fields[code].link = ['__contract'];
	__.queryIds[code] = (__.user.query && __.user.query.filter && __.user.query.filter.id) ? [__.user.query.filter.id] : [];
	callback();
}

exports.tpl = (_, d) => {

	_.__.global.form = 'contract';

	canChangeStatusRole = LST['contract~status'].list.obj[d.status]?.owner?.role;
	canChangeStatusCompany = LST['contract~status'].list.obj[d.status]?.owner?.company;
	isClientWorker = _.__.user.workerCompany === d.clientLink?.v;
	isExecutorWorker = _.__.user.workerCompany === d.executorLink?.v;
	canChangeStatus =
	canChangeStatusRole === 'main'
		? (
			(canChangeStatusCompany === 'client' && isClientWorker)
			||
			(canChangeStatusCompany === 'executor' && isExecutorWorker)
		)
		: canChangeStatusRole === _.__.user.role;

	return [

		['div', { class: 'test' }, [
			["div", { "class": "row" }, [
				["div", { "class": "col-md-4 col-sm-12" }, [
					["div", { "class": "ibox float-e-margins" }, [
						["div", { "class": "ibox-title" }, [
							["h5", {}, [
								["span", { "text": "Основная информация" }]
							]]
						]],
						["div", { "class": "ibox-content" }, [
							["div", { "class": "row" }, [
								["div", { "class": "col-sm-6 col-xs-12" }, [
									["div", { class: 'form-group' }, [
										["label", {}, [
											["span", { "text": 'Внутренний код' }],
										]],
										['div', {}, [
											_.f({ name: 'code', type: 'text--', value: '' }),
										]],
									]],
								]],
								["div", { "class": "col-sm-6 col-xs-12" }, [
									["div", { class: 'form-group bread-name', breadName: d.num }, [
										["label", {}, [
											["span", { "text": 'Номер' }],
										]],
										['div', {}, [
											_.f({ name: 'num', type: 'input-', value: '' }),
										]],
									]],
								]],
							]],
							["div", { "class": "row" }, [
								["div", { "class": "col-sm-6 col-xs-12" }, [
									["div", { class: 'form-group' }, [
										["label", {}, [
											["span", { "text": "Заказчик" }]
										]],
										['div', {}, [
											_.f({ name: 'clientLink', type: 'select2--' }),
										]],
									]],
								]],
								["div", { "class": "col-sm-6 col-xs-12" }, [
									["div", { class: 'form-group' }, [
										["label", {}, [
											["span", { "text": "Исполнитель" }]
										]],
										['div', {}, [
											_.f({ name: 'executorLink', type: 'select2--' }),
										]],
									]],
								]],
							]],
							["div", { class: 'form-group' }, [
								["label", {}, [
									["span", { "text": "Статус" }]
								]],
								['div', {}, [
									_.f({ name: 'status', type: 'select-', lst: 'contract~status', disabled: true }),
								]],

								// console.log({
								// 	contract: d,
								// 	role: _.__.user.role,
								// 	company: _.__.user.workerCompany,
								// 	LST: LST['contract~status'].list.obj,
								// 	canChangeStatus,
								// }),

								_.f({
									name: 'custom', sfx: 'changeStatus', type: 'action', config: {
										btn: {
											type: "button",
											text: 'Сменить статус',
											class: "btn btn-primary",
											disabled: !canChangeStatus,
										}
									}, process: {
										action: async function (conn, data, callback) {
											const contract = await conn.db.findOne('contract', data.field.parent._id, { status: 1, clientLink: 1, executorLink: 1 });
											const curStatusIndex = LST['contract~status'].list.lst.findIndex(l => l.v === contract.status);
											const nextStatus = LST['contract~status'].list.lst[curStatusIndex + 1]?.v;
											console.log({
												LST: LST['contract~status'].list.lst,
												userRole: conn.user.role,
												userCompany: conn.user.workerCompany,
												contract,
											});
											callback( OK({ nextStatus }) );
										}
									}, front: {
										// onClick: ($e, data) => {
										onAction: ($e, data) => {
											$e.attr('disabled', 'disabled');
											if(data.nextStatus){
												$('#formContent select[name=status]').val(data.nextStatus).trigger('change');
												setTimeout(()=>{ reloadForm($e) }, 1000);
											}
										},
									}
								}),
							]],
							["div", { class: 'form-group' }, [
								["label", {}, [
									["span", { "text": "Дата заключения" }]
								]],
								['div', {}, [
									_.f({ name: 'start_date', type: 'date', value: '-' }),
								]],
							]],
							["div", { class: 'form-group' }, [
								["label", {}, [
									["span", { "text": "Тип" }]
								]],
								['div', {}, [
									_.f({ name: 'type', type: 'select-', lst: 'contract~type', value: '' }),
								]],
							]],
							["div", { class: 'form-group' }, [
								["label", {}, [
									["span", { "text": "Комментарий" }]
								]],
								['div', {}, [
									_.f({ name: 'comm', type: 'textarea-', value: '' }),
								]],
							]],
						]]
					]]
				]],
				["div", { "class": "col-md-8 col-xs-12" }, [
					["div", { "class": "row" }, [
						["div", { "class": "col-xs-12" }, [
							["div", { "class": "ibox float-e-margins account-block" }, [
								["div", { "class": "ibox-title" }, [
									["h5", {}, [
										["span", { "text": "Банковские реквизиты" }]
									]]
								]],
								["div", { "class": "ibox-content" }, [
									_.html('bank/account~group', _, d),
								]]
							]],
						]],
					]],
					["div", { "class": "row" }, [
						["div", { "class": "col-xs-12" }, [
							_.html('__tpl~ibox', _, d, {
								title: 'Счета на оплату', content: (_, d) =>
									[
										_.html('payment~table', _, d, { filter: false }),
									]
							}),
						]],
					]],
				]]
			]],
			// ["div",{"class": "row"},[
			// 	["div",{"class": "col-xs-12"},[

			// 		["div",{"class": "ibox float-e-margins"},[
			// 			["div",{"class": "ibox-title"},[
			// 				["h5",{},[
			// 					["span",{"text": "Доходы по договору"}]
			// 				]]
			// 			]],
			// 			["div",{"class": "ibox-content"},[			

			// 				["div",{"class": "table-responsive"},[
			// 					["table",{"class": "table table-hover table-striped table-bordered"},[
			// 						["thead",{},[
			// 							["tr",{},[
			// 								["td",{},[
			// 									["span",{"text": "Дата"}],
			// 								]],
			// 								["td",{},[
			// 									["span",{"text": "Заявок"}],
			// 								]],
			// 								["td",{},[
			// 									["span",{"text": "Выездов"}],
			// 								]],
			// 								["td",{},[
			// 									["span",{"text": "Потенциальный доход"}],
			// 								]],
			// 								["td",{},[
			// 									["span",{"text": "Ожидаемый доход"}],
			// 								]],
			// 								["td",{},[
			// 									["span",{"text": "Потеряный доход"}],
			// 								]],
			// 							]]
			// 						]],
			// 						["tbody",{class: '*css*', style:()=>{/*css
			// 							.*css* .btn-lastitem {
			// 								position: absolute;
			// 								right: 20px;
			// 								bottom: 10px;
			// 								font-size: 0px;
			// 							}
			// 							.*css* .btn-lastitem:after {
			// 								content: 'Показать следующий 10 записей';
			// 								font-size: 16px;
			// 							}
			// 						css*/}},[
			// 							_.c({name: 'pay', add: false, filter: {l: -10, showOnButton: true}, process: {
			// 								parentDataNotRequired: true,
			// 								id: (__, code, callback)=>{

			// 									var field = __.fields[code];
			// 									var data = __.data[field.parent];

			// 									var sql = `
			// 										SELECT visit__date.f date, COUNT(visit__date.f) req_count, SUM(IF(pay__status.f = 'wait',1,0)) visit_count, SUM(IF(pay__status.f != 'null', pay__sum.f, 0)) draft_sum, SUM(IF(pay__status.f = 'wait', pay__sum.f, 0)) wait_sum, SUM(IF(pay__status.f = 'null', pay__sum.f, 0)) null_sum
			// 											FROM visit__date
			// 											LEFT JOIN visit__status ON visit__status.id = visit__date.id
			// 											LEFT JOIN pay_links__visit ON pay_links__visit.p = visit__date.id
			// 											LEFT JOIN pay__sum ON pay__sum.id = pay_links__visit.id
			// 											LEFT JOIN pay__status ON pay__status.id = pay_links__visit.id
			// 											LEFT JOIN pay_links__contract ON pay_links__contract.id = pay_links__visit.id
			// 											WHERE pay_links__contract.p = ? AND visit__status.f != 'error' AND visit__status.f != 'cancel'
			// 											GROUP BY date
			// 											ORDER BY date
			// 									`;
			// 									var escape = [data._id+''];

			// 									DB.selectWithFilter(__, code, callback, sql, escape);
			// 								},
			// 								tpl: (_, d)=>{ return [

			// 									["tr",{},[
			// 										["td",{},[
			// 											_.f({name: 'date', type: 'text-', value: '', front: {
			// 												prepareValue: (_)=>moment(_.value*1).format('LL'),
			// 											}}),
			// 										]],
			// 										["td",{},[
			// 											_.f({name: 'req_count', type: 'text-', value: ''}),
			// 										]],
			// 										["td",{},[
			// 											_.f({name: 'visit_count', type: 'text-', value: ''}),
			// 										]],
			// 										["td",{},[
			// 											_.f({name: 'draft_sum', type: 'text-', value: ''}),
			// 										]],
			// 										["td",{},[
			// 											_.f({name: 'wait_sum', type: 'text-', value: ''}),
			// 										]],
			// 										["td",{},[
			// 											_.f({name: 'null_sum', type: 'text-', value: ''}),
			// 										]],
			// 									]],
			// 								]},
			// 							}}, {tag: 'tr'}),
			// 						]]
			// 					]]
			// 				]],
			// 				["hr"],
			// 				["div",{"class": "row m-top-lg form-inline"},[

			// 				]]

			// 			]],
			// 		]],
			// 	]],
			// ]],
		]],
	]
}

exports.script = () => {

	window.updateMenuCustom = function (info) {

		var l = window.breadcrumb.length;

		if (l > 0) {
			delete window.breadcrumb[l - 1].active;
		} else {
			window.breadcrumb = [];
			window.breadcrumb.push({ query: { form: "contract~list", container: "formContent" }, text: 'Договоры' });
		}
		window.breadcrumb.push({
			text: 'Договор ' + $('#formContent .bread-name').attr('breadName'),
			query: history.state.subform, active: true,
		});
	};
}