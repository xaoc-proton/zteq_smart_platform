
exports.readme = {
	title: 'Список счетов на оплату',
	img: 'payment_list.png',
}

exports.tpl = (_, d, c = {})=>{

	if(c.hasParent === undefined) c.hasParent = true;

return [

	_.html('__tpl~table', _, d, {
		hasParent: c.hasParent,
		col: 'payment', 
		label: 'Счет на оплату',
		filter: c.filter != undefined ? c.filter : {
			items: [
				{class: "col-xs-3", f: {name: 'find_text', value: '', label: 'Поиск по номеру'}},
				{class: "col-xs-2", f: {name: 'find_from', type: 'date+', label: 'Дата создания с'}},
				{class: "col-xs-2", f: {name: 'find_to', type: 'date+', label: 'по'}},
				{class: "col-xs-2", f: {name: 'find_type', type: 'select', lst: 'payment~type', label: 'Тип счета'}},
				{class: "col-xs-2", f: {name: 'is_delete', type: 'check_btn+', label: 'Показывать\x0aудаленные'}},
			],
		},
		add: {
			modal: true,
			items: [
				{f: {name: 'num', label: "Номер счета"}},
				{f: {name: 'type', label: "Тип", type: 'select', lst: 'payment~type'}},
				{f: {name: 'date', label: "Дата счета", type: 'date+', value: ''}},
			],
			// afterValid: (conn, data, parents, callback)=>{
			// 	callback();
			// },
			beforeAdd: (conn, data, parents, callback)=>{
				// if(data.field.parent.config.hasParent){
				// 	conn.db.collection('ce')
				// 	.findOne({ _id: ObjectId(parents.find(item => item.col === 'ce')?._id) }, {name: 1}, (err, ce)=>{
				// 		data.tmp_obj.clientLink = {v: ce?._id?.toString(), l: ce?.name};
				// 		callback();
				// 	});
				// }else{
				// 	if(data.tmp_obj.clientLink){
				// 		parents.push({col: 'ce', _id: data.tmp_obj.clientLink.v});
				// 		callback();
				// 	}else{
				// 		return callback({status: 'err', err: 'Не указан объект привязки'});
				// 	}
				// }
				callback();
			},
		},
		table: {
			itemLink: {
				type: 'col',
			},
			// prepareItem: (_, d, item)=>{ 
			// 	if(d.delete_time){
			// 		item[1].class = (item[1].class||'')+' deleted';
			// 		item[1].help = 'Уволен '+moment(d.delete_time).format('LL');
			// 	}
			// 	return item;
			// },
			// checkItems: true,
			items: [
				{label: 'Добавлен', html: (_, d)=>[
					_.f({name: 'add_time', type: 'datetime--'}),
					_.if(d.delete_time, ()=>[
						['span', {text: 'Удален:'}],
						_.f({name: 'delete_time', type: 'datetime--'}),
					]),
				]},
				{label: 'Номер', f: {name: 'num', type: 'label'}},
				{label: 'Тип', f: {name: 'type', type: 'select--', lst: 'payment~type'}},
				{label: 'Дата счета', f: {name: 'date', type: 'date--', value: ''}},
				{label: 'Акцепт', f: {name: 'accept', type: 'check--'}},
			],
			id: {
				baseField: 'add_time',
				prepare: (select)=>{
					
					select.join.push("LEFT JOIN "+select.config.col+"__delete_time ON "+select.config.col+"__delete_time.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
					if(!select.filter.is_delete) select.where.push(select.config.col+"__delete_time.f = 0");
					
					if(select.filter.find_from){
						select.where.push(select.config.col+"__"+select.config.table.id.baseField+".f >= ?");
						select.escape.push(moment(select.filter.find_from).unix()*1000);
					}
					if(select.filter.find_to){
						select.where.push(select.config.col+"__"+select.config.table.id.baseField+".f < ?");
						select.escape.push(moment(select.filter.find_to).add(1,'day').unix()*1000);
					}

					if(select.filter.find_text){
						select.join.push("LEFT JOIN "+select.config.col+"__num ON "+select.config.col+"__num.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
						select.where.push(select.config.col+"__num.f LIKE ?");
						select.escape.push('%'+select.filter.find_text.trim()+'%');
					}
					if(select.filter.find_type){
						select.join.push("LEFT JOIN "+select.config.col+"__type ON "+select.config.col+"__type.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
						select.where.push(select.config.col+"__type.f = ?");
						select.escape.push(select.filter.find_type);
					}
				},
			},
		}
	}),
]} 