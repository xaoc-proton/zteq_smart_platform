
exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	allow: ['payment']
}, callback);

exports.readme = {
	title: 'Форма работы со счетами',
	img: 'payment_form.png',
}

exports.id = (__, code, callback)=>{
	__.fields[code].col = 'payment';
	__.fields[code].link = ['__payment'];
	__.queryIds[code] = (__.user.query && __.user.query.filter && __.user.query.filter.id) ? [__.user.query.filter.id] : [];
	callback();
}

exports.tpl = (_, d)=>{
	
	_.__.global.form = 'payment';

return [

	['div', {}, [

		["div",{"class": "row"},[
			["div",{"class": "col-xs-4"},[
				["div",{"class": "ibox float-e-margins"},[
					["div",{"class": "ibox-title"},[
						["h5",{},[
							["span",{"text": "Основная информация"}]
						]],
						_.if(_.editMode, ()=>[
							["div",{class: `css
								position: absolute;
								right: 30px;
							`},[
								_.f({name: 'delete_time', type: 'check_btn', value: '', label: 'Удален', config: {saveTime: true}}),
							]],
						]),
					]],
					["div",{"class": "ibox-content"},[
						["div",{class: 'form-group'},[
							["label",{},[
								["span",{"text": 'Внутренний код'}],
							]],
							['div', {}, [
								_.f({name: 'code', type: 'text--', value: ''}),
							]],
						]],
						["div",{class: 'form-group bread-name', breadName: d.num},[
							["label",{},[
								["span",{"text": 'Номер'}],
							]],
							['div', {}, [
								_.f({name: 'num', type: 'input-', value: ''}),
							]],
						]],
						["div",{class: 'form-group'},[
							["label",{},[
								["span",{"text": "Дата счёта"}]
							]],
							['div', {}, [
								_.f({name: 'date', type: 'date', value: '-'}),
							]],
						]],
						["div",{class: 'form-group'},[
							["label",{},[
								["span",{"text": "Тип"}]
							]],
							['div', {}, [
								_.f({name: 'type', type: 'select-', lst: 'payment~type', value: ''}),
							]],
						]],
						["div",{class: 'form-group'},[
							["label",{},[
								["span",{"text": "Комментарий"}]
							]],
							['div', {}, [
								_.f({name: 'comm', type: 'textarea-', value: ''}),
							]],
						]],
					]]
				]]
			]],
			["div",{"class": "col-xs-8"},[

			]]
		]],
	]],
]}

exports.script = ()=>{
	
	window.updateMenuCustom = function(info){
		
		var l = window.breadcrumb.length;

		if(l > 0){
			delete window.breadcrumb[l-1].active;
		}else{
			window.breadcrumb = [];
			window.breadcrumb.push({query: {form:"payment~list", container:"formContent"}, text:'Счета на оплату'});
		}
		window.breadcrumb.push({
			text: 'Счет на оплату '+$('#formContent .bread-name').attr('breadName'),
			query: history.state.subform, active: true,
		});
	};
}