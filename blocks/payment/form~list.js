
exports.config = {
	menu: {label: 'Счета на оплату', icon: 'fas fa-address-book'},
}

exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	allow: ['payment']
}, callback);

exports.id = async (__, code, callback)=>{
	__.fields[code].col = 'user';
	__.fields[code].link = ['__'+__.fields[code].col];
	__.queryIds[code] = [__.user.key];
	callback();
}

exports.tpl = (_, d)=>{ return [

	_.html('__tpl~list', _, d, {
		content: (_, d)=>[
					
			_.html('__tpl~table', _, d, {
				hasParent: false,
				col: 'payment', 
				label: 'Счет на оплату',
				filter: c.filter != undefined ? c.filter : {
					items: [
						{class: "col-xs-3", f: {name: 'find_text', value: '', label: 'Поиск по номеру'}},
						{class: "col-xs-2", f: {name: 'find_from', type: 'date+', label: 'Дата создания с'}},
						{class: "col-xs-2", f: {name: 'find_to', type: 'date+', label: 'по'}},
						{class: "col-xs-2", f: {name: 'find_type', type: 'select', lst: 'payment~type', label: 'Тип счета'}},
						{class: "col-xs-2", f: {name: 'is_delete', type: 'check_btn+', label: 'Показывать\x0aудаленные'}},
					],
				},
				add: false,
				table: {
					itemLink: {
						type: 'col',
					},
					items: [
						{label: 'Добавлен', html: (_, d)=>[
							_.f({name: 'add_time', type: 'datetime--'}),
							_.if(d.delete_time, ()=>[
								['span', {text: 'Удален:'}],
								_.f({name: 'delete_time', type: 'datetime--'}),
							]),
						]},
						{label: 'Номер', f: {name: 'num', type: 'label'}},
						{label: 'Тип', f: {name: 'type', type: 'select--', lst: 'payment~type'}},
						{label: 'Дата счета', f: {name: 'date', type: 'date--', value: ''}},
						{label: 'Акцепт', f: {
							name: 'accept', 
							type: _.__.user.role === 'head' ? 'check+' : 'check--',
						}},
						{label: 'Договор', html: (_, d)=>[
							_.c({name: 'contract', add: false, process: {
								tpl: (_, d)=>[
									_.f({name: 'num', type: 'json-'}),
									['span', {text: 'Договор '+d.num}],
									_.c({name: 'ce', add: false, process: {
										tpl: (_, d)=>[
											_.f({name: 'name', type: 'json-'}),
											['span', {text: ' с '+d.name}],
										],
									}}),
								],
							}}),
						]}
					],
					id: {
						baseField: 'add_time',
						prepare: (select)=>{
							
							select.join.push("LEFT JOIN "+select.config.col+"__delete_time ON "+select.config.col+"__delete_time.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
							if(!select.filter.is_delete) select.where.push(select.config.col+"__delete_time.f = 0");
							
							if(select.filter.find_from){
								select.where.push(select.config.col+"__"+select.config.table.id.baseField+".f >= ?");
								select.escape.push(moment(select.filter.find_from).unix()*1000);
							}
							if(select.filter.find_to){
								select.where.push(select.config.col+"__"+select.config.table.id.baseField+".f < ?");
								select.escape.push(moment(select.filter.find_to).add(1,'day').unix()*1000);
							}
		
							if(select.filter.find_text){
								select.join.push("LEFT JOIN "+select.config.col+"__num ON "+select.config.col+"__num.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
								select.where.push(select.config.col+"__num.f LIKE ?");
								select.escape.push('%'+select.filter.find_text.trim()+'%');
							}
							if(select.filter.find_type){
								select.join.push("LEFT JOIN "+select.config.col+"__type ON "+select.config.col+"__type.id = "+select.config.col+"__"+select.config.table.id.baseField+".id");
								select.where.push(select.config.col+"__type.f = ?");
								select.escape.push(select.filter.find_type);
							}
						},
					},
				}
			}),

		]
	}),
]}

exports.script = ()=>{

}

exports.style = ()=>{/*

*/}