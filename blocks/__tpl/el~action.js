
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['action'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return (data.config||{}).btn ? [
		["button", Object.assign(data.config.btn, {code: data.code})],
	] : [] },
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){ doAfterLoad.push((e,d)=>{

			if(!data.front) data.front = {};

			if(!(data.config||{}).btn){
				if(realParent.attr('code')){
					console.error("Элемент el~action создан некорректно. У realParent уже установлен code и не может быть переписан.", data);
				}else{
					// без этого будет передаваться некорректная ссылка на элемент в onAction и других front-события...
					realParent.attr('code', data.code);
				}
			}

			if(data.front.onClick){
				
				( (data.config||{}).btn 
					? realParent.find('button[code='+data.code+']')
					: realParent
				).off('click').on('click', function(e){ window[data.front.onClick]( $(e.target) ) });
			
			}else{
				
				( (data.config||{}).btn 
					? realParent.find('button[code='+data.code+']')
					: realParent
				).off('click').on('click', function(e){
					
					var $target = $(e.target);

					if(!$target.attr('code')) $target = $target.parent().closest('[code]');

					if($target.attr('disabled')){
						
						$.notify('Предыдущее действие еще не завершено', {className: 'warn'});
					
					}else{
						
						$target.attr('disabled', 'disabled');
						
						wsSendCallback({action: '', code: data.code}, function(res){
							
							$target.removeAttr('disabled');
							if(data.front && data.front.onAction) window[data.front.onAction]( $target, res );
						
						}, function(res){
							
							$target.removeAttr('disabled');
							if(data.front && data.front.onError) window[data.front.onError]( $target, res );
							
							if($target.length){
								$target.notify(res.errMsg, {position: $target.attr('notifyPosition'), className: 'warn'});
							}else{
								$.notify(res.errMsg, {className: 'warn'});
							}
						});
					}
				});
			}
		}) },
	},
}

exports['action+'] =
exports['action-'] =
exports['action--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_action'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_action'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}