
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['img'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){

		if(!data.value) data.value = {};
		data.src = data.value.l || '';
	
	return [

		['div',{class: data.class+" "},[
			!data.label ? [] : ['label',{},[
				['span',{text: data.label}],
			]],
			['div', {type: 'img', style:`
				background-image: url(`+(data.value.l||'')+`);
				background-size: contain;
				background-repeat: no-repeat;
				background-position: center center;
				width: 100%;
				height: 100%;
			`}],
		]],
		
	]}
}

exports['img+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		data.img = {accept:"image/*", capture: true};
	
	return [

		['div',{class: data.class+" "},[

			window.el['__tpl~el_file'].tpl.bind(this)(_, d, data, tpl),
			
			['div', {type: 'img', code: data.code, style:`
				background-image: url(`+(data.value.l||'')+`);
				background-size: contain;
				background-repeat: no-repeat;
				background-position: center center;
				width: 100%;
				height: 100%;
			`}],
		]],
		
	]},
}

exports['img-'] = 
exports['img--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_img'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}