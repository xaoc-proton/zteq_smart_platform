
exports.config = {
	menu: {label: 'Справочники', icon: 'fa fa-list'},
}

exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	//allow: ['lst_editor']
}, callback);

exports.id = DB.getContent;

exports.tpl = (_, d)=>{ return [
	
	['div', {class: 'row'}, [
	
		['style', {src:'blocks/__tpl/lst_editor/static/libs/jsoneditor/jsoneditor.min.css'}],
		['script', {src:'blocks/__tpl/lst_editor/static/libs/jsoneditor/jsoneditor.min.js'}],
	
		['div', {id: 'lstList', class: 'col-xs-3'}, [
		
			_.script(()=>{
				$(document).off('click', '[lstCode]').on('click', '[lstCode]', function(e)
				{
					if(!$(e.target).attr('lstCode')) return; // клики на детей не обрабатываем (часть бизнес-логики)

					$('[lstCode]').removeClass('active');
					const $item = $(this).closest('[lstCode]');
					const lstCode = $item.attr('lstCode');
					const $complex = $('#formContent .complex-lstForm');
					reloadComplex($complex, {code: lstCode, branch: $item.find('input:checked').val() || false}, ()=>{
						$item.addClass('active');
					});
				});
			}),
			
			Object.entries(LST).map(([code, list])=>
			{
				const branches = Object.entries(list.branch || {});
			return [
				['div', {
					text: code, 
					lstCode: code,
					class: `css
						.*css* {
							cursor: pointer;
						}
						.*css*:not(.active):hover {
							opacity: 0.5;
						}
						.*css*.active {
							cursor: default;
						}
						.*css*.active {
							border: 1px solid;
							border-radius: 10px;
							padding: 5px;
						}
						.*css*:not(.active) > * {
							display: none;
						}
					`
				}, [
					_.if(branches.length > 1, ()=>[
						_.f({name: 'lstBranch.'+code, sfx: code, type: 'radio', label: 'Доступные\xa0версии:', config: {
							lst: branches.map(([name, branch])=>({v: name, l: branch.label}))
						}, front: {
							afterSave: ($e, d)=>{
								$e.closest('[lstcode]').trigger('click');
							},							
						}, process: {
							save: (conn, field, parent, saveData, cb)=>{
								SYS.set(DB.__content.lstBranch, field.sfx, saveData.value)
								SYS.updateLST({prepare: {key: field.sfx+'__'+saveData.value+'_ver', link: LST[field.sfx].branch[saveData.value].path}});
								cb();
							},
						}}),
					]),
				]]
			]}),
		]],

		['div', {class: 'col-xs-9'}, [
			_.c({name: 'lstForm', col: 'lst', add: false, process: {
				parentDataNotRequired: true,
				id: (__, code, callback)=>
				{
					const field = __.fields[code];
					__.fields[code].col = 'null';
					__.queryIds[code] = [ Object.assign({_id: true}, field.filter||{}) ];
					callback();
				},
				tpl: (_, d)=>
				[
					['div', {}, [
						_.if(!d.code, ()=>
						[
							['center', {text: 'Выберите справочник для редактирования'}],
						]),
						_.if(d.code, ()=>
						[
							_.f({name: 'fakeLst', type: 'select', lst: d.code, class: 'hidden'}),							
							
							['div', {id: 'lstEditor', code: d.code}],
							
							_.if((d.branch === false || d.branch === 'user'), ()=>
							[
								_.f({name: '__tpl/lst_editor~update', type: 'action', config: {
									btn: {
										type: "button", 
										text: 'Сохранить',
										class: "btn btn-xs"+`css
											position: absolute;
											top: -50px;
											right: 0px;
											z-index: 100001;
										`
									}
								}, front: {
									onClick: $btn=>{
										$btn.attr('disabled', 'disabled');
										wsSendCallback({action: '', code: $btn.attr('code'), data: window.editor.get()}, ok=>{
											$btn.removeAttr('disabled');
										}, err=>{ $btn.removeAttr('disabled') });
									},
								}}),
							]),
						]),
					]],
				],
			}, front: {
				onItemLoad: ()=>{
					window.afterAllLoaded.push( ()=>
					{
						const container = $('#lstEditor');
						const lstCode = container.attr('code');
						
						if(lstCode)
						{
							const lst = LST[lstCode];
							if(!lst){
								notifyError( {err: `Справочник ${lstCode} не найден`} );
							}else
							{
								window.editor = new JSONEditor(container[0], {
									//mode: 'tree',
									//modes: ['code', 'form', 'text', 'tree', 'view', 'preview'], // allowed modes
									enableTransform: false,
									onEvent: function(node, event)
									{
										if(event.type == 'input')
										{
											console.log(node, event, this, window.editor.get());
										}
										else if(event.type == 'click' && (node.value+'').indexOf('function')!=-1)
										{
											//c1('...a', ...a);
											const $hiddenParent = $('#hiddenParent');
											const $modalChild = $hiddenParent.find('> *');
											window.editor.code.setValue( node.value );

											$('#modalCodeEditorClose').one('click', ()=>{
												
												let child = window.editor.node;
												node.path.forEach(_=>{
													//c1('child', child, '_=', _);
													if(typeof _ == 'string'){
														child = child.childs.filter(__=>__.field==_)[0];
													}else{
														child = child.childs[_];
													}
													//c1('child+', child, '_=', _);
												});
												child.value = window.editor.code.getValue();
												window.editor.refresh();
												
												$modalChild.appendTo( $hiddenParent );
											});
											$modalChild.appendTo( $('#lstEditor') );
										}
									}
								});
								window.editor.set( JSON.parse( JSON.stringify(lst, (k, v)=>(typeof v == 'function' ? v.toString() : v) )) );
								window.editor.code = ace.edit( $('#modalCodeEditor')[0] );
							}
						}
					} );
				},
			}}),
		]],
		
		['div', {id: 'hiddenParent', class: 'hidden'}, [
			['button', {id: 'modalCodeEditorClose', class: `css
				position: absolute;
				top: 0px;
				right: 0px;
				z-index: 100001;
			`, text: 'Закрыть'}],
			['div', {id: 'modalCodeEditor', class: `css
				position: absolute;
				top: 0px;
				left: 0px;
				z-index: 100000;
				width: 100%;
				height: 100%!important;
				border: 2px solid;
			`}],
		]],
	]],
]}

exports.script = ()=>{

}

exports.func = ()=>{

}

exports.style = ()=>{/*

*/}