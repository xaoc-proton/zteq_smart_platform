
exports.config = {
	multi: true,
	customName: true,
}

exports['list'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{ return [
		["div",{"class": "ibox"},[
			["div",{"class": "ibox-content"},[
				c.content(_, d),
			]],
		]],
	]},
	script: ()=>{
		
	},
	style: ()=>{/*
	*/},
}