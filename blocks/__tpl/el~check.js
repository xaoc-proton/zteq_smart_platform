
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['check'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		data.type = 'checkbox';
		if(data.value) data.checked = true;
		
		if(!data.config) data.config = {};
		if(data.config.content == undefined) data.config.content = 'Нет|Да'; // проверка на undefined, т.к. может прийти false (без content)
		
		if(data.config.content){
			data.config.content = data.config.content.split('|');
			data['content-text'] = data.config.content[0];
			data['content-text-checked'] = data.config.content[1];
		}
		
		data.saveTime = data.config.saveTime;
		
	return [

		['div',{class: data.class+" "+(data.config.content===false?'no-text':'')},[
			['label',{for: data.code+'_check'},[
				['span',{text: data.label}],
			]],
			['input', Object.assign({}, data, {id: data.code+'_check', class: ''})],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			this.save = function($e, data, callback){
				data.value = $e.is(':checked') ? ( $e.attr('saveTime') ? Date.now() : 1 ) : 0;
				callback(true, data);
			}
			
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('input')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}
		},
	},
	style: ()=>{/*
		.el > input[type=checkbox] {
			width: 16px;
			height: 16px;
			margin-right: 100%;
		}
		.el > input[type=checkbox]:after {
			content: attr(content-text);
			margin-left: 20px;
		}
		.el > input[type=checkbox]:checked:after {
			content: attr(content-text-checked);
		}
		.el.no-text > input[type=checkbox]:after {
			content: '';
		}
	*/},
}

exports['check+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_check'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_check'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['check-'] =
exports['check--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.disabled = 'disabled';
	
	return [

		window.el['__tpl~el_check'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}

exports['check_btn'] = exports['check_btn+'] = exports['check_btn-'] = exports['check_btn--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
		window.el['__tpl~el_check'].tpl.bind(this)(_, d, data, tpl),
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			
			window.el['__tpl~el_check'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
			
			tpl[0][1].class += ' check-style-btn';
			tpl[0][2].unshift( tpl[0][2].pop() );
		},
	},
	style: ()=>{/*
		.el.check-style-btn > input {
			display: none;
		}
		.el.check-style-btn > label {
			border: 1px solid;
			padding: 4px 14px 4px 14px;
			border-radius: 4px;
			margin-bottom: 20px;
			font-size: 10px;
			font-weight: normal;
			text-align: center;
			white-space: pre-wrap;
		}
		.el.check-style-btn > input + label {
			color: #888;
		}
		.el.check-style-btn > input:checked + label {
			color: #1d84c6;
			font-weight: bold;
		}
	*/},
}