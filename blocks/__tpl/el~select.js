
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['select'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		['div',{class: data.class},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['select', data, [
				this.element = function(e){ return [
					['option', {text: e.l, value: e.v, selected: e.v == data.value}]
				]},
			]],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			
			const prepareEl = this.element;
			
			(window.LST[data.lst] ? doAfterLoad : afterAllLoaded).push(function(){

				var $e = realParent.find('select[code='+data.code+']');
				if($e.find('.elParent').length) $e = $e.find('.elParent'); // не проверял, сделал по аналогии с radio
				
				if(!window.LST[data.lst]){
					window.LST[data.lst] = [{v:'',l:''}];
					window.oLST[data.lst] = {};
				}
				
				if(typeof data.value == 'object'){
					
					if(!window.oLST[data.lst][data.value.v]){
						if(data.multiple){
							// пока нет живых кейсов с кастомными multiple-значениями, не совсем понятно что тут делать...
						}else{
							window.oLST[data.lst][data.value.v] = data.value;
							window.LST[data.lst].push(data.value);
						}
					}
					
					data.value = data.value.v;
					$e.attr('value', data.value);
				}
				
				if(prepareEl && $e.length && data.lst && window.LST[data.lst]){
					window.LST[data.lst].forEach(function(l,i){
						if(!l.hide) tplToHtml(prepareEl(l), $e, '', doAfterLoad, realParent);
					});
				}
			});
		}
	},
}

exports['select+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_select'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_select'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['select-'] = 
exports['select--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}