
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['radio'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		['div',{class: data.class, code: data.code},[
			
			['label',{},[
				['span',{text: data.label}],
			]],
			
			this.element = function(e){ return [
				['p', {}, [
					['input', Object.assign({}, data, {type: 'radio', name: data.name+'_'+data.code, id: data.code+'_'+e.v, checked: e.v == data.value, value: e.v})],
					['label', {text: e.l, for: data.code+'_'+e.v}],
				]]
			]},
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			
			const prepareEl = this.element;
			const lst = data.config.lst || window.LST[data.lst];
			
			(lst ? doAfterLoad : afterAllLoaded).push(function(){

				var $e = realParent.find('.el[code='+data.code+']');
				if($e.find('.elParent').length) $e = $e.find('.elParent');

				if(prepareEl && $e.length && lst && !$e.attr('prepareEl'))
				{
					$e.attr('prepareEl', true); // костыль - защита от дублирования (причину не искал)
					
					lst.forEach(function(l,i){
						if(!l.hide) tplToHtml(prepareEl(l), $e, '', doAfterLoad, realParent);
					});
				}
			});
		}
	},
	script: ()=>{
		
		// !!! в таком виде это дублирует событие change для input (если этот кусок где-то используется, то надо переделать)
		
		// $(document).off('click', 'input[type="radio"] + label');
		// $(document).on('click', 'input[type="radio"] + label', function(e) {
			// var $el = $( e.currentTarget.control );
			// $el.closest('.el').find('input').removeAttr('checked');
			// $el.attr('checked', true).trigger('change');
		// });
		
	},
}

exports['radio+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_radio'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_radio'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['radio-'] = 
exports['radio--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}