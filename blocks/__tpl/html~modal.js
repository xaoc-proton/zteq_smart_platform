
exports.config = {
	multi: true,
	customName: true,
}

exports['modal'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{
	
		if(!c.add.items) c.add.items = [];
	
	return [

		["div",{"class": "modal fade tpl-theme-modal","role": "dialog","id": c.col+"-modal"},[
			c.add.modal.html ? c.add.modal.html(_, d) : [
				_.if(_.editMode, ()=>[
					_.c({name: 'tmp_obj_'+c.col, col: 'tmp_obj', config: c, process: {
						tpl: (_, d)=>{
						
							if(_.__.pre){
								Object.keys(_.config.add||[]).forEach((key)=>{
									if(typeof _.config.add[key] == 'function'){
										_.__.process[_.linecode+'_'+key] = {action: _.config.add[key]};
									}
								});
							}
						
							_.config.add.items.filter(_=>_).forEach((item, key)=>{
								if(_.__.pre){
									if(item.html){
										_.__.process[_.linecode+'_add_item_'+key] = {tpl: item.html};
									}
								}else{
									if(_.__.process[_.linecode+'_add_item_'+key]) item.html = _.__.process[_.linecode+'_add_item_'+key].tpl;
								}
							});
							
						return [
							
							["div",{"class": "modal-dialog","role": "document"},[
								
								["div",{"class": "modal-content"},[
									
									["div",{"class": "modal-header"},[
										["button",{"type": "button","class": "close","data-dismiss": "modal","aria-label": "Close"},[
											["span",{"aria-hidden": "true"},[
												["i",{"class": "fas fa-times"}]
											]]
										]],
										["h4",{"class": "modal-title"},[
											["span", {"text": "Новый "+(_.config.label||'')}],
										]]
									]],
									
									["div",{"class": "modal-body"},[
										["div",{"role": "form", class: 'm-t'},[
										
											_.config.add.items.filter(_=>_).map(item=>[]
												.concat(item.f ? [_.f( item.f )] : [])
												.concat(item.html ? item.html(_, d) : [])
											),
											
										]],
									]],
									
									["div",{"class": "modal-footer"},[
										["button",{"type": "button","class": "btn btn-default","data-dismiss": "modal"},[
											["span",{"text": "Отменить"}]
										]],
										["button",{"type": "button","class": "btn btn-primary"},[
											["span",{"text": "Создать"}],
											_.f({name: '__tpl~modalAdd', type: 'action', process: (()=>{
												var obj = {};
												(SYS.get(_, 'config.add.required')||[]).forEach((item, key)=>{
													if(typeof item == 'function') obj['required_'+key] = item;
												});
												return obj;
											})(),front: {
												onAction: ()=>location.reload(),
												onError: ($e)=>reloadComplex($e),
											}}),
										]]
									]],
								]],
							]],
						]},
					}}),
				]),
			],	
		]],
		
	]},
	script: ()=>{
		$('.tpl-theme-modal').on('shown.bs.modal', function(e){
			if(window.resizeAllTextarea) window.resizeAllTextarea($(e.target));
		});
	},
	 style: ()=>{/*
		.tpl-theme-modal {
			position: fixed!important;
		}
		.tpl-theme-modal .el > .select2 {
			min-width: 100%;
		}
	*/},
}

exports["modalAdd"] = {
	config: {
		customType: "action",
		customName: key=>key, // поиск функции идет без '.js', который по дефолту добавляется в SYS.updateBLOCK
	},
	f: function (conn, data, callback) {

		async.waterfall(
		[
			data.getParent.bind(["field.parent.parent", "parent"]),
			data.get1.bind(["tmp_obj", data.field.parent._id]),
			cb =>
			{
				data.form = SYS.requireFile("cache", data.field.parent.form + (conn.user.role ? "_" + conn.user.role : "") + (data.field.theme?"_" + data.field.theme:''));

				var check = (SYS.get(data.field.parent.config, "add.required") || [])
					.map((item, key) =>
						item || data.form.process[data.field.parent.linecode][`required_${key}__${data.field.name}`](data)
					)
					.filter(_ =>
						!( _ != undefined && !_.err
							? _ : SYS.get(data.tmp_obj, _.f + (typeof data.tmp_obj[_.f] == "object" ? ".v" : "")) )
					);

				cb(!check.length ? null : {status: "err", err: check.map(_ => _.err).join("; ")});
			},
			cb =>
			{
				data.parentLinks = [data.parent.parent];

				var afterValid = SYS.get(data.form.process[data.field.parent.linecode + "_afterValid"], "action");

				async.waterfall(
					// все что после валидации (в т.ч. дополнительная валидация) кладем сюда
					afterValid ? [cb => { afterValid(conn, data, data.parentLinks, cb) }] : [],
					err => { if (err) { cb(err) } else
					{
						DB.deleteComplex(conn, data.field.parent, data.parentLinks, cb);
					}}
				);
			},
			cb =>
			{
				conn.db.collection("__content")
					.findAndModify(
						{ _id: DB.__content._id }, [],
						{ $inc: { ["counters." + data.field.parent.config.col]: 1 } },
						{ fields: { counters: 1 } },
						(err, findAndModify) =>
						{
							data.tmp_obj.code = (SYS.get(findAndModify, "value.counters." + data.field.parent.config.col)||0) + 1;

							const beforeAdd = SYS.get(data.form.process[data.field.parent.linecode + "_beforeAdd"], "action");

							async.waterfall(
								// если требуется изменить объект перед добавлением, то делаем это тут
								beforeAdd ? [cb => { beforeAdd(conn, data, data.parentLinks, cb)}] : [],
								err => { if (err) { cb(err)} else 
								{
									delete data.tmp_obj.add_time;
									delete data.tmp_obj._id;
									
									const afterAdd = SYS.get(data.form.process[data.field.parent.linecode + "_afterAdd"], "action");
									
									DB.addComplex(conn, {col: data.field.parent.config.col}, data.parentLinks, data.tmp_obj, (
										afterAdd ? () => { afterAdd(data, cb) } : cb
									));
								}}
							);
						}
					);
			},
		], err => { callback(err || { status: "ok" }) });
	},
};
