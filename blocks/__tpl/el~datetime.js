
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['datetime'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){

		data.type = 'datetime-local'; // принудительно подключаем дефолтный датапикер браузера
		if(data.value) data.value = moment(new Date(data.value)).format('Y-MM-DDTHH:mm'); // дефолтный формат браузера
	
	return [

		['div',{class: data.class+" "},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['input', Object.assign({}, data, {class: 'el-value', autocomplete: 'off'})],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			this.save = function($e, data, callback){
				data.value = moment(data.value).unix()*1000;
				callback( data.value ? true : {err: 'Дата не распознана'}, data );
			}
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('input')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}

		}
	},
}

exports['datetime+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_datetime'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_datetime'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['datetime-'] = 
exports['datetime--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		if(data.value) data.value = moment(new Date(data.value)).format('LLL');
	
	return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}