
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['input'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		if(data.stype == 'filter'){
			data.placeholder = data.label+'';
			data.labelHide = true;
		}	
	return [

		['div',{class: data.class+" form-group"},[
			data.labelHide ? [] : ['label',{},[
				['span',{text: data.label}],
			]],
			['input', Object.assign({}, data, {class: 'el-value form-control'})],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_input'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['input+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_input'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_input'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['input-'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}

exports['input--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}