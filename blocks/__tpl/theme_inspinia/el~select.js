
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['select'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){

		if(data.stype == 'filter'){
			data.placeholder = data.label+'';
			data.labelHide = true;
		}
	return [

		['div',{class: data.class+" form-group"},[
			data.labelHide ? [] : ['label',{},[
				['span',{text: data.label}],
			]],
			['select', Object.assign({}, data, {class: 'form-control'}), [
				this.element = function(e){ return [
					['option', {text: e.l, value: e.v, selected: e.v == data.value}]
				]},
			]],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_select'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['select+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_select'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_select'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['select-'] = 
exports['select--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}