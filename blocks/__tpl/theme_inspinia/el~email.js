
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['email'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.type = 'input';
	
	return [

		window.el['__tpl/theme_inspinia~el_input'].tpl.bind(this)(_, d, data, tpl),

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_email'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['email+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_email'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_email'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['email-'] = 
exports['email--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_email'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}