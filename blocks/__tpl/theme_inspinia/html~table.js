
exports.tpl = (_, d, c)=>{

	if(!c.filter) c.filter = {};
	if(!c.filter.items) c.filter.items = [];
	c.filter.items = c.filter.items.filter(_=>_);
	if(c.filter.items.length && !c.filter.findBtn) c.filter.findBtn = {};
	
	if(c.add === true) c.add = {modal: true};
	if(c.add){
		if(!c.add.btn) c.add.btn = {};
	}
	
	if(!c.table) c.table = {};
	if(!c.table.filter) c.table.filter = -10;
	if(!c.table.items) c.table.items = [];
	c.table.items = c.table.items.filter(_=>_);
	if(!c.table.itemLink) c.table.itemLink = {};
	if(!c.table.itemLink.type) c.table.itemLink.type = 'row';
	if(!c.table.itemLink.link) c.table.itemLink.link = {form: c.col+'~main', container: 'formContent'};

return [
	
	!(c.add||{}).modal ? [] : _.html('__tpl~modal', _, d, c),
	
	["div",{"class": "bPageFilters"},[
		["div",{"class": "row"},[
			c.filter.items.map(item=>
				["div",{"class": item.class || "col-xs-3"},[
					_.f( Object.assign(item.f||{}, {stype: 'filter'}) ),
				]]
			)
			.concat(!c.filter.findBtn ? [] : (c.filter.findBtn.html || [
				["div",{"class": c.filter.findBtn.class||"col-xs-1"},[
					['div', {text: c.filter.findBtn.label||'Найти', class: 'btn btn-primary'}, [
						_.f({name: 'find_btn', type: 'action', front: {
							onClick: (_)=>{
								var filter = {};
								_.closest('.ibox').find('.filter-field .form-control').map(function(i,f){
									if(f.classList.contains("hasDatepicker")){
										filter[f.name] = f.value.replace( /(\d{2})\.(\d{2})\.(\d{4})/, "$3-$2-$1");
									}else{
										filter[f.name] = f.type == 'checkbox' ? f.checked : f.value;
									} 
								});
								filter.force = true;
								reloadComplex(_.closest('.ibox').find('tbody'), filter);
							}
						}}),
					]],
				]]
			]))
			.concat(!c.add ? [] : (c.add.btn.html || [
				_.if(_.editMode, ()=>[
					["div",{"class": c.add.btn.class || "text-right"},[
						["div",{id: 'btn-new-'+c.col, "class": "btn btn-primary","data-toggle": "modal","data-target": "#"+c.col+"-modal"},[
							["span",{"text": c.add.btn.label || "Новый "+(c.label||'')}]
						]]
					]],
				]),
			]))
		]],
	]],
	["div",{"class": "table-responsive"},[
		["table",{id: c.col+'-table', "class": "table table-hover table-striped table-bordered"},[
			["thead",{},[
				["tr",{},[
					( !c.table.checkItems ? [] : [
						["td",{"class": "min"},[
							_.f({name: 'checkAllItems', type: 'check', stype: 'filter', config: {content: false}, front: {
								onFilter: ($e)=>{
									$e.closest('table')
										.find('[name=checkItem]')
										.prop('checked', $e.prop('checked')?'checked':'')
										.trigger('change');
								}
							}}),
						]]
					]).concat(c.table.items
						.concat(c.table.itemLink.type != 'col' ? [] : {label: '', class: 'min'})
						.map(item=>
							["td",{class: item.class||''},[
								["span",{"text": item.label||""}],
							]],
						)
					)
				]]
			]],
			_.html('__tpl~table_body', _, d, Object.assign(c, {
				row: {content: (_, d)=>[
					_.config.table.prepareItem(_, d, 
						
						["tr", _.config.table.itemLink.type == 'row'  ? {class: 'h', query: _.config.table.itemLink.query} : {}, [
							
							( !_.config.table.checkItems ? [] : [
								["td",{itemId: d._id},[
									_.f({name: 'checkItem', type: 'check', stype: 'filter', config: {content: false}}),
								]]
							]).concat(
								_.config.table.items.map(item=>["td",{class: item.class||''},[]
									
									.concat(item.f ? [_.f( item.f )] : [])
									
									.concat(item.c ? [_.c( {name: item.c.name, add: false, config: {f: item.c.f}, process: {
										tpl: (_, d)=>(_.config.f ? [ _.f( _.config.f ) ] : [])
									}})] : [])
									
									.concat(item.html ? item.html(_, d) : [])
									
								])
							).concat(
								[_.config.table.itemLink.type != 'col' ? [] : ["td", {class: 'edit h', query: _.config.table.itemLink.query}]]
							),
						]],
					)
				]}
			})),
		]]
	]],
	["hr"],
	["div",{"class": "row m-top-lg form-inline"},[
		
	]]
	
]};

exports.script = ()=>{

}

exports.style = ()=>{/*
	.table .max100 { max-width: 100px }
	.table .max150 { max-width: 150px }
	.table .max200 { max-width: 200px }
*/}