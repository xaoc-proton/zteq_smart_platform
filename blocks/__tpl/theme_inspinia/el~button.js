
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['button'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		if(!data.config) data.config = {};
		if(!data.config.type) data.config.type = 'default';
		if(!data.config.actionData) data.config.actionData = {};
	
		delete data.type; // ����� ������������ type: "button" �� ����
	
	return [

		['button', Object.assign({}, data, {type: "button", class: "btn btn-"+data.config.type}, data.config.actionData),[
			(data.label && data.label.icon
				? ["span",{"aria-hidden": "true"},[ ["i",{"class": data.label.icon}] ]]
				: ['span', {text: data.label}]
			),
		]],
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_button'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		},
	},
}

exports['button+'] =
exports['button-'] =
exports['button--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_button'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_button'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}