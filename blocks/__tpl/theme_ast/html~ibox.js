
exports.config = {
	multi: true,
}

exports['html'] = {
	tpl: (_, d, c)=>{ return [

		["div",{class: "card","no-body": ""},[
			["div",{class: "card-header "+(c.titleClass||'')},[
				["div",{class: "row align-items-center"},[
					["h"+(c.titleSize||3),{class: "col-auto"},[
						["span",{text: c.title||''}]
					]],
					["div",{class: "col-auto ml-auto d-flex align-items-center"},[
						["button",{type: "button", "aria-expanded": c.hide ? false : true, class: "btn p-0 m-n2 text font-weight-500 btn-link btn-icon"},[
							["div",{class: "sh-icon sh-icon-plus", style: !c.hide ? 'display: none;' : ''},[
								["div",{class: "sh-icon-wraper",style: "width: 16px; height: 16px;"},[
									["svg",{viewBox: "0 0 6 6",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
										["path",{d: "M5.66667 2.66667H3.33333V0.333329C3.33333 0.149328 3.18333 0 3 0C2.816 0 2.66667 0.149328 2.66667 0.333329V2.66667H0.333329C0.149328 2.66667 0 2.816 0 3C0 3.18333 0.149328 3.33333 0.333329 3.33333H2.66667V5.66667C2.66667 5.85 2.816 6 3 6C3.18333 6 3.33333 5.85 3.33333 5.66667V3.33333H5.66667C5.85 3.33333 6 3.18333 6 3C6 2.816 5.85 2.66667 5.66667 2.66667Z",class: "fill"}]
									]]
								]]
							]],
							["div",{class: "sh-icon sh-icon-minus", style: c.hide ? 'display: none;' : ''},[
								["div",{class: "sh-icon-wraper",style: "width: 16px; height: 2px;"},[
									["svg",{viewBox: "0 0 16 2",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
										["path",{d: "M8.88888 0H15.1111C15.6 0 16 0.447989 16 1C16 1.55 15.6 2 15.1111 2H8.88888H0.888876C0.398207 2 0 1.55 0 1C0 0.447989 0.398207 0 0.888876 0H8.88888Z",class: "fill"}]
									]]
								]]
							]],
						]]
					]]
				]]
			]],
			["div",{class: "collapse "+(!c.hide?'show':'')},[
				["div",{class: "card-body "+(c.contentClass||'')}, [
					c.content(_, d),
				]]
			]],
		]],
	]},
	script: ()=>{
		
		$(document).off('click', '.card-header button[aria-expanded]');
		$(document).on('click', '.card-header button[aria-expanded]', function()
		{
			const $this = $(this);
			const $card = $(this).closest('.card');
			
			if($this.attr('aria-expanded') === 'true'){
				$this.attr('aria-expanded', 'false');
				$this.find('> .sh-icon-minus').hide();
				$this.find('> .sh-icon-plus').show();
				$card.find('> .collapse').removeClass('show');
			}else{
				$this.attr('aria-expanded', 'true');
				$this.find('> .sh-icon-plus').hide();
				$this.find('> .sh-icon-minus').show();
				$card.find('> .collapse').addClass('show');
			}
		});
	}
}