
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['select2'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl)
	{
		data.type = 'select2';
	return [

		['div',{class: data.class+" "},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['select', Object.assign({}, data||{}, {
				config: JSON.stringify(data.config||{}),
			}), [
				this.element = function(e){ return [
					['option', {
						text: e.l, value: e.v, 
						selected: data.multiple 
							? (data.value||[]).indexOf(e.v) != -1 
							: typeof data.value == 'object' 
								? (data.value||[]).filter(_=>(_||{}).v == e.v).length
								: e.v == data.value
					}]
				]},
			]],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			window.el['__tpl~el_select'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
			
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('select')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}
			
			(($.fn.select2 && window.LST[data.lst]) ? doAfterLoad : afterAllLoaded).push(function()
			{
				const $e = realParent.find('select[code='+data.code+']');
				var ajax = data.ajax ? (data.ajax+'').split('|') : false;
				
				if($e.length && data.lst && window.LST[data.lst]){
					
					setTimeout(function(){
						
						const p = ajax ? {
							minimumInputLength: ajax[1] != undefined ? ajax[1]*1 : 3,
							ajax: {
								delay: 500,
								/*transport: function (params, success, failure) {
									wsSendCallback( {
										action: 'search_'+data.lst, q: params.data.q, code: data.code,
									}, success, failure );
								},*/
								transport: function (params, success, failure) {
									var promise = new Promise(function(resolve, reject) {
										wsSendCallback( {
											action: 'search_'+data.lst, q: params.data.q, code: data.code,
										}, function(searchData){
											searchData.results = searchData.answer.results;
											if(!searchData.results) searchData.results = [];
											
											// текстовые значения (label) могли поменяться - обновляем
											searchData.results.forEach(function(d){
												// это надо переделать, чтобы данные select2 (и значение внутри select2-selection__rendered) тоже обновлялись
												if(d.id) $e.find('option[value="'+d.id+'"]').text(d.text);
											});

											if(data.lst == 'addobj'){
												data.addOption = Object.assign({id: "process_new", text: 'Добавить', icon: 'XAOC/images/addNew.png'}, data.addOption);
												searchData.results.unshift(data.addOption);
											}
											
											resolve(searchData);
										}, reject );
									});
									promise.then(success);
									promise.catch(failure);
								},
							},
						} : {
							minimumInputLength: 0
						};
						
						p.language = "ru";
						p.allowClear = true;
						p.placeholder = '';
						let config = $e.attr('config');
						
						if(config)try{
							config = JSON.parse(config);
							if(config.select2) p = extend(p, config.select2);
						}catch(e){}

						if(typeof window[data.prepare] == 'function') window[data.prepare](p);
						
						$e.select2(p);
						
						// топорно, но так требует документация select2
						if(p.multiple) $e.val( JSON.parse($e.attr('value')||'[]') ).attr('fakeChange',true).trigger('change');
					}, 0);
				}
			});
			
			// если поставить этот блок прямо перед предыдущим - можно словить чудо-баг
			this.save = function($e, data, callback)
			{
				data.value = window.getValueFromSelect2($e, data);
				callback(true, data);
			}
		}
	},
	func: ()=>{

		window.getValueFromSelect2 = function($e, data)
		{
			let value;
			// существует проблема с обновлением текстового значения (если оно обновилось в сущности). Прямо сейчас - обнулить, обновить страницу (обнулятся LST), подгрузить новое.
			if(((($e.data('select2')||{}).options||{}).options||{}).multiple){
				value = [];
				$e.find(':selected').each(function(){ if(this.value) value.push({ v: this.value, l: (this.innerHTML||'').trim() }) });
			}else{
				value = {v: data.value, l: $e.find(':selected').text().trim()};
			}
			return value;
		}
	},
	script: ()=>{

		loadRes('XAOC/select2/select2.full.min.js', true, function(){
			
			loadRes('XAOC/select2/select2.min.css', true);

			if($.fn.select2){

				$.fn.select2.amd.define('select2/i18n/ru',[],function () {

					// Russian
					return {
						errorLoading: function () {
							return 'Результат не может быть загружен.';
						},
						inputTooLong: function (args) {
							var overChars = args.input.length - args.maximum;
							var message = 'Пожалуйста, удалите ' + overChars + ' символ';
							if (overChars >= 2 && overChars <= 4) {
								message += 'а';
							} else if (overChars >= 5) {
								message += 'ов';
							}
							return message;
						},
						inputTooShort: function (args) {
							var remainingChars = args.minimum - args.input.length;

							var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';

							return message;
						},
						loadingMore: function () {
							return 'Загружаем ещё ресурсы…';
						},
						maximumSelected: function (args) {
							var message = 'Вы можете выбрать ' + args.maximum + ' элемент';

							if (args.maximum  >= 2 && args.maximum <= 4) {
								message += 'а';
							} else if (args.maximum >= 5) {
								message += 'ов';
							}

							return message;
						},
						noResults: function () {
						  return 'Ничего не найдено';
						},
						searching: function () {
						  return 'Поиск…';
						}
					};
				});
			}
		});
		
		window.select2_obj_0 = function(p){	
			function s2_obj (state) {
				if(!state.id) return state.text;
				if(state.element) return (state.context?state.context+' - ':'')+state.text;
				var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
				return $state;
			};
			
			p.templateResult = s2_obj;
			p.templateSelection = s2_obj;
			p.minimumInputLength = 0;
			p.dropdownCssClass = 'select2_obj';
		}
		
		window.select2_obj_1 = function(p){	
			function s2_obj (state) {
				if(!state.id) return state.text;
				if(state.element) return (state.context?state.context+' - ':'')+state.text;
				var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
				return $state;
			};
			
			p.templateResult = s2_obj;
			p.templateSelection = s2_obj;
			p.minimumInputLength = 1;
			p.dropdownCssClass = 'select2_obj';
		}

		window.select2_obj = function(p){	
			function s2_obj (state) {
				if(!state.id) return state.text;
				if(state.element) return state.text;
				var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
				return $state;
			};
			
			p.templateResult = s2_obj;
			p.templateSelection = s2_obj;
			p.minimumInputLength = 3;
			p.dropdownCssClass = 'select2_obj';
		}
		
		window.select2_obj_full_text = function(p){	
			function s2_obj (state) {
				if(!state.id) return state.text;
				if(state.element) return (state.context?state.context+' - ':'')+state.text;
				var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
				return $state;
			};
			
			p.templateResult = s2_obj;
			p.templateSelection = s2_obj;
			p.minimumInputLength = 3;
			p.dropdownCssClass = 'select2_obj';
		}

		window.select2_obj_inline = function(p){
			function select2_obj_inline (state) {
				if(!state.id) return state.text;
				if(state.element) return (state.context?state.context+' - ':'')+state.text;
				var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
				return $state;
			};
			
			p.templateResult = select2_obj_inline;
			p.templateSelection = select2_obj_inline;
			p.minimumInputLength = 3;
			p.dropdownCssClass = 'select2_obj_inline';
		}
	},
}

exports['select2+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_select2'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_select2'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['select2-'] =
exports['select2--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}