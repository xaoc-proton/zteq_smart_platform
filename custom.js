/* project local script (e.c. cron-script) */

customGlobal = function(conn, data, callback){try{
	
	var w = [];
	
	if(BLOCK.action) w = Object.keys(BLOCK.action)
		.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
		.filter(_=>_.customGlobal)
		.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
		.map(_=>cb=>
		{
			console.log('customGlobal ::', _.info || _.__path);
			_.f.call(data, conn, Object.assign({customGlobal: true}, data), answer=>
			{
				if(answer && answer.err) console.log(answer.err);
				cb();
			});
		});
	
	async.waterfall(w.concat([]), ()=>{
		if(typeof callback == 'function') callback();
	});
	
}catch(e){
	console.log(e);
	if(typeof callback == 'function') callback();
}}

customLocal = function(conn, data, callback){try{

	var w = [];
	
	if(BLOCK.action) w = Object.keys(BLOCK.action)
		.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
		.filter(_=>_.customLocal)
		.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
		.map(_=>cb=>
		{
			console.log('customLocal ::', _.info || _.__path);
			_.f.call(data, conn, Object.assign({customLocal: true}, data), answer=>
			{
				if(answer && answer.err) console.log(answer.err);
				cb();
			});
		});
	
	async.waterfall(w.concat([]), ()=>{
		if(typeof callback == 'function') callback();
	});

}catch(e){
	console.log(e);
	if(typeof callback == 'function') callback();
}}

cronGlobal = function(conn, data, callback){try{

	var w = [];
	
	if(BLOCK.action) w = Object.keys(BLOCK.action)
		.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
		.filter(_=>_.cronGlobal)
		.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
		.map(_=>cb=>
		{
			console.log('cronGlobal ::', _.info || _.__path);
			_.f.call(data, conn, Object.assign({cronGlobal: true}, data), answer=>
			{
				if(answer && answer.err) console.log(answer.err);
				cb();
			});
		});
	
	async.waterfall(w.concat([]), ()=>{
		if(typeof callback == 'function') callback();
	});
	
}catch(e){
	console.log(e);
	if(typeof callback == 'function') callback();
}}

cronGlobal = function(conn, data, callback){try{

	var w = [];
	
	if(BLOCK.action) w = Object.keys(BLOCK.action)
		.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
		.filter(_=>_.cronLocal)
		.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
		.map(_=>cb=>
		{
			console.log('cronLocal ::', _.info || _.__path);
			_.f.call(data, conn, Object.assign({cronLocal: true}, data), answer=>
			{
				if(answer && answer.err) console.log(answer.err);
				cb();
			});
		});
	
	async.waterfall(w.concat([]), ()=>{
		if(typeof callback == 'function') callback();
	});

}catch(e){
	console.log(e);
	if(typeof callback == 'function') callback();
}}